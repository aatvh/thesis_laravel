<?php
/**
 * @created 01.10.13 12:25
 * @package tests\unit\models
 * @author Sebastian Löwe
 */

class UserrolesTest extends TestCase{
    protected function getFixture(){
        return new Userroles();
    }

    public function testCanCreateModel(){
        $model = $this->getFixture();

        $this->assertInstanceOf('Userroles', $model);

        return $model;
    }

    /**
     * @depends testCanCreateModel
     * @param Userroles $model
     * @return Userroles
     */
    public function testCanFillModel(Userroles $model){
        $values = array(
            'user_id'   => 1,
            'role'      => 'admin'
        );

        $model->fill($values);

        $this->assertEquals($values, $model->toArray());

        return $model;
    }

    /**
     * @depends testCanFillModel
     * @param Userroles $model
     */
    public function testCanGetUserId(Userroles $model){
        $this->assertEquals(1, $model->getUserId());
    }

    /**
     * @depends testCanFillModel
     * @param Userroles $model
     */
    public function testCanGetRole(Userroles $model){
        $this->assertEquals('admin', $model->getRole());
    }
}