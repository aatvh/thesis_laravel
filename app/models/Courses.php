<?php
/**
 * @created 01.10.13 12:04
 * @package models
 * @author Sebastian Löwe
 */

use Illuminate\Database\Eloquent\Model AS Eloquent;

class Courses extends Eloquent {

    protected $table = 'courses';

    protected $fillable = array(
        'name',
        'description'
    );

    /**
     * @param $description
     * @return Courses
     */
    public function setDescription($description){
        $this->attributes['description'] = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(){
        return array_key_exists('description', $this->attributes)?$this->attributes['description']:null;
    }

    /**
     * @param $name
     * @return Courses
     */
    public function setName($name){
        $this->attributes['name'] = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(){
        return array_key_exists('name', $this->attributes)?$this->attributes['name']:null;
    }
}