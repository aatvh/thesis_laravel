<?php
/**
 * @created 01.10.13 12:25
 * @package tests\unit\models
 * @author Sebastian Löwe
 */

class UsercoursesTest extends TestCase{
    protected function getFixture(){
        return new Usercourses();
    }

    public function testCanCreateModel(){
        $model = $this->getFixture();

        $this->assertInstanceOf('Usercourses', $model);

        return $model;
    }

    /**
     * @depends testCanCreateModel
     * @param Usercourses $model
     * @return Usercourses
     */
    public function testCanFillModel(Usercourses $model){
        $values = array(
            'user_id'           => 1,
            'course_id'         => 2,
            'registration_at'   => '2013-10-10 10:10:10'
        );

        $model->fill($values);

        $this->assertEquals($values, $model->toArray());

        return $model;
    }

    /**
     * @depends testCanFillModel
     * @param Usercourses $model
     */
    public function testCanGetUserId(Usercourses $model){
        $this->assertEquals(1, $model->getUserId());
    }

    /**
     * @depends testCanFillModel
     * @param Usercourses $model
     */
    public function testCanGetCourseId(Usercourses $model){
        $this->assertEquals(2, $model->getCourseId());
    }

    /**
     * @depends testCanFillModel
     * @param Usercourses $model
     */
    public function testCanGetRegisteredAt(Usercourses $model){
        $registered = $model->getRegistrationDate();

        $this->assertInstanceOf('Datetime', $registered);
        $this->assertEquals('2013-10-10 10:10:10', $registered->format('Y-m-d H:i:s'));
    }
}