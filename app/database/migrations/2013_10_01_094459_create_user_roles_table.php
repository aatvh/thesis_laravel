<?php

use Illuminate\Database\Migrations\Migration;

class CreateUserRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_roles', function($table){
            $table->integer('user_id');
            $table->string('role',8);

            $table->timestamps();

            $table->primary(array('user_id','role'));
            $table->foreign('user_id')->on('user','id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_roles');
	}

}