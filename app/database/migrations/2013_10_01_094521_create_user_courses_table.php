<?php

use Illuminate\Database\Migrations\Migration;

class CreateUserCoursesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_courses', function($table){
            $table->integer('user_id');
            $table->integer('course_id');

            $table->datetime('registration_at');
            $table->integer('points')->nullable();

            $table->timestamps();

            $table->primary(array('user_id','course_id'));
            $table->foreign('user_id')->on('user','id');
            $table->foreign('course_id')->on('courses_semester','id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_courses');
	}

}