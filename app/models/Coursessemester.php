<?php
/**
 * @created 01.10.13 12:06
 * @package models
 * @author Sebastian Löwe
 */

use Illuminate\Database\Eloquent\Model AS Eloquent;

class Coursessemester extends Eloquent {

    protected $table = 'courses_semester';

    protected $fillable = array(
        'course_id',
        'semester',
        'prof_id'
    );

    /**
     * @return int|null
     */
    public function getCourseId(){
        return array_key_exists('course_id', $this->attributes)?$this->attributes['course_id']:null;
    }

    /**
     * @return int|null
     */
    public function getProfId(){
        return array_key_exists('prof_id', $this->attributes)?$this->attributes['prof_id']:null;
    }

    /**
     * @return int|null
     */
    public function getSemester(){
        return array_key_exists('semester', $this->attributes)?$this->attributes['semester']:null;
    }
}
