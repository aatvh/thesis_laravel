<?php
/**
 * @created 01.10.13 12:25
 * @package tests\unit\models
 * @author Sebastian Löwe
 */

class UserTest extends TestCase{
    protected function getFixture(){
        return new User();
    }

    public function testCanCreateUser(){
        $user = $this->getFixture();

        $this->assertInstanceOf('User', $user);

        return $user;
    }

    /**
     * @depends testCanCreateUser
     * @param User $user
     * @return User
     */
    public function testCanFillUser(User $user){
        $values = array(
            'gender'    => 'mister',
            'surname'   => 'Fox',
            'firstname' => 'Ian'
        );

        $user->fill($values);

        $this->assertEquals($values, $user->toArray());

        return $user;
    }

    /**
     * @depends testCanFillUser
     * @param User $user
     */
    public function testCanSetFirstname(User $user){
        $firstname = 'Jon';

        $this->assertEquals($firstname, $user->setFirstName($firstname)->getFirstName());
    }

    /**
     * @depends testCanFillUser
     * @param User $user
     */
    public function testCanSetGender(User $user){
        $gender = 'mister';

        $this->assertEquals($gender, $user->setGender($gender)->getGender());
    }

    /**
     * @depends testCanFillUser
     * @param User $user
     */
    public function testCanSetSurname(User $user){
        $surname = 'Doe';

        $this->assertEquals($surname, $user->setSurname($surname)->getSurname());
    }

    /**
     * @depends testCanFillUser
     * @param User $user
     */
    public function testCanSetUsername(User $user){
        $username = 'jondoe';

        $this->assertEquals($username, $user->setUsername($username)->getUsername());
    }

    /**
     * @depends testCanFillUser
     * @param User $user
     */
    public function testCanGetAuthIdentifier(User $user){
        $username = 'jondoe';
        $user->setUsername($username);

        $this->assertEquals($username, $user->getAuthIdentifier());
    }
}