<?php
/**
 * @created 01.10.13 12:25
 * @package tests\unit\models
 * @author Sebastian Löwe
 */

class CoursesTest extends TestCase{
    protected function getFixture(){
        return new Courses();
    }

    public function testCanCreateModel(){
        $model = $this->getFixture();

        $this->assertInstanceOf('Courses', $model);

        return $model;
    }

    /**
     * @depends testCanCreateModel
     * @param Courses $model
     * @return Courses
     */
    public function testCanFillModel(Courses $model){
        $values = array(
            'name'          => 'Some event',
            'description'   => 'Event description'
        );

        $model->fill($values);

        $this->assertEquals($values, $model->toArray());

        return $model;
    }

    /**
     * @depends testCanFillModel
     * @param Courses $model
     */
    public function testCanSetName(Courses $model){
        $name = 'Another course';

        $this->assertEquals($name, $model->setName($name)->getName());
    }

    /**
     * @depends testCanFillModel
     * @param Courses $model
     */
    public function testCanSetDescription(Courses $model){
        $description = 'Another description';

        $this->assertEquals($description, $model->setDescription($description)->getDescription());
    }
}