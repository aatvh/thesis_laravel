<?php
/**
 * @created 01.10.13 12:04
 * @package models
 * @author Sebastian Löwe
 */

use Illuminate\Database\Eloquent\Model AS Eloquent;

class Userroles extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_roles';

    protected $primaryKey = array('user_id','role');

    protected $fillable = array(
        'user_id',
        'role'
    );

    public $incrementing = false;

    public function getUserId(){
        return array_key_exists('user_id', $this->attributes)?$this->attributes['user_id']:null;
    }

    public function getRole(){
        return array_key_exists('role', $this->attributes)?$this->attributes['role']:null;
    }
}