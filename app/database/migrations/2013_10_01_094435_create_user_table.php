<?php

use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user',function($table){
            $table->increments('id');
            $table->string('gender',8);
            $table->string('surname',64);
            $table->string('firstname',64);
            $table->string('username',32);
            $table->string('status',8);

            $table->string('password',64)->nullable();

            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user');
	}

}