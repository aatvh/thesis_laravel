<?php
/**
 * @created 01.10.13 12:25
 * @package tests\unit\models
 * @author Sebastian Löwe
 */

class CoursessemesterTest extends TestCase{
    protected function getFixture(){
        return new Coursessemester();
    }

    public function testCanCreateModel(){
        $model = $this->getFixture();

        $this->assertInstanceOf('Coursessemester', $model);

        return $model;
    }

    /**
     * @depends testCanCreateModel
     * @param Coursessemester $model
     * @return Coursessemester
     */
    public function testCanFillModel(Coursessemester $model){
        $values = array(
            'course_id' => 1,
            'semester'  => 20131,
            'prof_id'   => 2
        );

        $model->fill($values);

        $this->assertEquals($values, $model->toArray());

        return $model;
    }

    /**
     * @depends testCanFillModel
     * @param Coursessemester $model
     */
    public function testCanGetCourseId(Coursessemester $model){
        $this->assertEquals(1, $model->getCourseId());
    }

    /**
     * @depends testCanFillModel
     * @param Coursessemester $model
     */
    public function testCanGetSemester(Coursessemester $model){
        $this->assertEquals(20131, $model->getSemester());
    }

    /**
     * @depends testCanFillModel
     * @param Coursessemester $model
     */
    public function testCanGetProfId(Coursessemester $model){
        $this->assertEquals(2, $model->getProfId());
    }
}