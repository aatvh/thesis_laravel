<?php
/**
 * @created 01.10.13 12:07
 * @package models
 * @author Sebastian Löwe
 */

use Illuminate\Database\Eloquent\Model AS Eloquent;

class Usercourses extends Eloquent {

    protected $table = 'user_courses';

    protected $fillable = array(
        'user_id',
        'course_id',
        'registration_at'
    );

    public $incrementing = false;

    protected $primaryKey = array('user_id','course_id');

    /**
     * @return int|null
     */
    public function getCourseId(){
        return array_key_exists('course_id', $this->attributes)?$this->attributes['course_id']:null;
    }

    /**
     * @return int|null
     */
    public function getPoints(){
        return array_key_exists('points', $this->attributes)?$this->attributes['points']:null;
    }

    /**
     * @param $points
     * @return Usercourses
     */
    public function setPoints($points){
        $this->attributes['points'] = $points;

        return $this;
    }

    /**
     * @return Datetime|null
     */
    public function getRegistrationDate(){
        return array_key_exists('registration_at', $this->attributes)?new DateTime($this->attributes['registration_at']):null;
    }

    /**
     * @return int|null
     */
    public function getUserId(){
        return array_key_exists('user_id', $this->attributes)?$this->attributes['user_id']:null;
    }
}