<?php
/**
 * @created 01.10.13 12:04
 * @package models
 * @author Sebastian Löwe
 */

use Illuminate\Auth\UserInterface;
use Illuminate\Database\Eloquent\Model AS Eloquent;

class User extends Eloquent implements UserInterface {

	protected $table = 'user';

    protected $fillable = array(
        'gender',
        'surname',
        'firstname'
    );

	protected $hidden = array('password');

	public function getAuthIdentifier()
	{
		return $this->getUsername();
	}

	public function getAuthPassword()
	{
		return $this->password;
	}

    /**
     * @return string|null
     */
    public function getFirstName(){
        return array_key_exists('firstname', $this->attributes)?$this->attributes['firstname']:null;
    }

    /**
     * @param $firstName
     * @return User
     */
    public function setFirstName($firstName){
        $this->attributes['firstname'] = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getGender(){
        return array_key_exists('gender', $this->attributes)?$this->attributes['gender']:null;
    }

    /**
     * @param $gender
     * @return User
     */
    public function setGender($gender){
        $this->attributes['gender'] = $gender;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSurname(){
        return array_key_exists('surname', $this->attributes)?$this->attributes['surname']:null;
    }

    /**
     * @param $surname
     * @return User
     */
    public function setSurname($surname){
        $this->attributes['surname'] = $surname;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUsername(){
        return array_key_exists('username', $this->attributes)?$this->attributes['username']:null;
    }

    /**
     * @param $username
     * @return User
     */
    public function setUsername($username){
        $this->attributes['username'] = $username;

        return $this;
    }
}