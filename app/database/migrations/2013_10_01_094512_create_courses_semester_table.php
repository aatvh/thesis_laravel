<?php

use Illuminate\Database\Migrations\Migration;

class CreateCoursesSemesterTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('courses_semester', function($table){
            $table->increments('id');
            $table->integer('course_id');
            $table->integer('semester');
            $table->integer('prof_id');

            $table->timestamps();
            $table->unique(array('course_id','semester'));
            $table->foreign('prof_id')->on('user','id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('courses_semester');
	}

}